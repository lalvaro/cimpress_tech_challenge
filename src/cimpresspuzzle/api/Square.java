package cimpresspuzzle.api;

public class Square {
	public Square(int x, int y, int size) {
		this.x = x;
		this.y = y;
		this.size = size;
	}
	public int x;
	public int y;
	public int size;
}
