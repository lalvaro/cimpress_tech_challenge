package cimpresspuzzle.api;

import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;

public class Puzzle implements Cloneable {

	public static final int CELL_EMPTY = 1;
	public static final int CELL_BLOCKED = 0;

	private String id;
	private int[] grid;
	private int height;
	private int width;

	public Puzzle() {
		grid = null;
		height = 0;
		width = 0;
	}

	@Override
	public Puzzle clone() {
		return new Puzzle(this);
	}

	public Puzzle(Puzzle copy) {
		grid = copy.grid.clone();
		height = copy.height;
		width = copy.width;
		id = copy.id;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int[] getData() {
		return grid;
	}

	public String getId() {
		return id;
	}

	public Puzzle(String filePath) {
		id = "";
		height = 0;
		width = 0;

		try {
			byte[] encoded = Files.readAllBytes(Paths.get(filePath));
			String json = new String(encoded);

			parseJSON(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void parseJSON(String input) {
		Gson gson = new Gson();
		JSONPuzzle puzzle = gson.fromJson(input, JSONPuzzle.class);

		this.height = Integer.parseInt(puzzle.height);
		this.width = Integer.parseInt(puzzle.width);
		this.id = puzzle.id;

		grid = new int[height * width];

		for (int h = 0; h < height; h++) {
			for (int w = 0; w < width; w++) {
				grid[w + (h * width)] = puzzle.puzzle[h][w] ? CELL_EMPTY : CELL_BLOCKED;
			}
		}
	}
}
