package cimpresspuzzle.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import com.google.gson.Gson;

public class CimpressPuzzleAPI {

	private static final String PRIVATEKEY = "58a4c18576724d0eb7cb307ea99712b4"; // FINAL KEY


	private static final String TRIAL_GETPUZZLE_URL 	= "http://techchallenge.cimpress.com/" + PRIVATEKEY + "/trial/puzzle";
	private static final String CONTEST_GETPUZZLE_URL 	= "http://techchallenge.cimpress.com/" + PRIVATEKEY + "/contest/puzzle";

	private static final String TRIAL_ANSWER_URL 		= "http://techchallenge.cimpress.com/"+ PRIVATEKEY + "/trial/solution";
	private static final String CONTEST_ANSWER_URL 	= "http://techchallenge.cimpress.com/"+ PRIVATEKEY + "/contest/solution";

	private static final boolean CONTEST = false;


	public static Puzzle getNextPuzzle() {

		URL trialURL;

		try {

			trialURL = new URL(CONTEST? CONTEST_GETPUZZLE_URL : TRIAL_GETPUZZLE_URL);
			URLConnection yc = trialURL.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
			String inputLine;
			String result = "";

			while ((inputLine = in.readLine()) != null) {
				result += inputLine;
			}
			in.close();

			Puzzle ret = new Puzzle();
			ret.parseJSON(result);

			return ret;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void sendPuzzleSolution(PuzzleSolution info, String puzzleId) {

		JSONPuzzleAnswer answer = new JSONPuzzleAnswer(info, puzzleId);

		Gson gson = new Gson();

		try {
			sendPost(gson.toJson(answer));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 {
	     "id": "71ec6d9997be4821b2e38e7b5506f96d",
	     "squares":
	     [
	       { "X": 3, "Y": 5, "Size": 2 },
	       { "X": 5, "Y": 5, "Size": 1 },
	       ...
	     ]
	   }
	 */

	private static void sendPost(String data) throws Exception {

		String url = CONTEST? CONTEST_ANSWER_URL : TRIAL_ANSWER_URL;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add request header
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = data;

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		@SuppressWarnings("unused")
		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		if (response.toString().contains("timePenalty\":0,")) {
			System.out.println(response.toString());
		} else {
			System.err.println(response.toString());
			System.exit(-1);
		}

	}
}
