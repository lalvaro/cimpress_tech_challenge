package cimpresspuzzle.api;

import java.util.ArrayList;

public class JSONPuzzle {
	class dataLine {
		public ArrayList<String> values;
	}

	public String width;
	public String height;
	public boolean[][] puzzle;
	public String id;
}