package cimpresspuzzle.api;

import java.util.ArrayList;

public class JSONPuzzleAnswer {

	static class SquareInfo {
		int X;
		int Y;
		int Size;
		
		public SquareInfo(Square sq) {
			this.X = sq.x;
			this.Y = sq.y;
			this.Size = sq.size;
		}
		
	}
	
	private String id;
	private SquareInfo[] Squares;
	
	public JSONPuzzleAnswer(PuzzleSolution info, String puzzleId) {
		
		ArrayList<Square> solution = info.getSolution();
		int nbSquares = solution.size();
		Squares = new SquareInfo[nbSquares];
		
		for(int i=0; i<nbSquares; i++) {
			Squares[i] = new SquareInfo(solution.get(i));
		}
		
		this.id = puzzleId;
	}
}
