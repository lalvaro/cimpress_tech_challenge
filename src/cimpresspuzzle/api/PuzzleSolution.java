package cimpresspuzzle.api;

import java.util.ArrayList;

public class PuzzleSolution {

	private ArrayList<Square> solution;

	public PuzzleSolution() {
		solution = new ArrayList<Square>();
	}

	public int getNbSquaresFound() {
		if (solution == null) {
			return Integer.MAX_VALUE;
		}
		return solution.size();
	}

	public void addSquare(Square sq) {
		this.solution.add(sq);
	}

	public ArrayList<Square> getSolution() {
		return this.solution;
	}
}
