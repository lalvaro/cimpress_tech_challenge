package cimpresspuzzle.solver;

import cimpresspuzzle.api.Puzzle;
import cimpresspuzzle.api.PuzzleSolution;

public class ThreadedAlgorithm {

	private PuzzleSolverAlgorithm solver;
	private PuzzleSolution bestSolution;
	private Thread thread;

	public ThreadedAlgorithm(PuzzleSolverAlgorithm solver) {
		this.solver = solver;
	}

	private void init(Puzzle data) {
		this.bestSolution = null;
		solver.init(data);
	}

	public void start(Puzzle puzzle) {

		if (thread != null) {
			while (thread.isAlive()) {
				Thread.yield();
			}
		}

		init(puzzle);

		thread = new Thread(new Runnable() {

			@Override
			public void run() {

				while (true) {

					try {
						PuzzleSolution info = solver.compute();
						if ((bestSolution == null) || (bestSolution.getNbSquaresFound() > info.getNbSquaresFound())) {
							bestSolution = info;
						}
						if (Thread.currentThread().isInterrupted()) {
							break;
						}
					} catch (InterruptedException e) {
						break;
					}
				}
			}
		}, solver.getName());

		thread.start();
	}

	public void stop() {
		if (thread != null) {
			thread.interrupt();
		}
	}

	public PuzzleSolution getResults() {
		return bestSolution;
	}
}
