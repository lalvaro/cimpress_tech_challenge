package cimpresspuzzle.solver;

import cimpresspuzzle.api.Puzzle;
import cimpresspuzzle.api.PuzzleSolution;

public interface PuzzleSolverAlgorithm {
	String getName();

	void init(Puzzle data);

	PuzzleSolution compute() throws InterruptedException;

}
