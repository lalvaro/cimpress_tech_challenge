package cimpresspuzzle.solver;

import java.util.Random;

public class PuzzleSolverConfiguration {
	
	private static final int FRAGMENTATION_MIN_SIZE	= 3;  
	private static final int FRAGMENTATION_RANGE 	= 3;	
	public  static final int MAX_SQUARE_SIZE    	= 100;	
	
	public static int generateWeightingFragmentationParameter() {
		Random rnd = new Random();
		return rnd.nextBoolean() ? FRAGMENTATION_MIN_SIZE + rnd.nextInt(FRAGMENTATION_RANGE) : MAX_SQUARE_SIZE;
	}

	private static final float LOWEST_SIZE_PRIORITY	= 1.3f;  
	private static final float SIZE_PRIORITY_RANGE 	= 0.4f;	
	
	public static float generateSizePriorityParameter() {
		Random rnd = new Random();		
		return LOWEST_SIZE_PRIORITY + (SIZE_PRIORITY_RANGE * rnd.nextFloat());
	}

	private static final float LOWEST_TOLERANCE	= 0.005f;  
	private static final float TOLERANCE_RANGE 	= 0.004f;
	
	public static float generateToleranceParameter() {
		Random rnd = new Random();		
		return LOWEST_TOLERANCE + (TOLERANCE_RANGE * rnd.nextFloat());
	}
}