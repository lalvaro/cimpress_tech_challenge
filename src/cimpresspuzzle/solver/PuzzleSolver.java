package cimpresspuzzle.solver;

import java.util.ArrayList;
import java.util.List;

import cimpresspuzzle.api.Puzzle;
import cimpresspuzzle.api.PuzzleSolution;
import cimpresspuzzle.solver.algo.WeightingAlgorithm;

public class PuzzleSolver {

	private List<ThreadedAlgorithm> launcherList = new ArrayList<ThreadedAlgorithm>();

	public PuzzleSolver() {

		// Prepare solver launchers.
		int nbProcs = Runtime.getRuntime().availableProcessors();
		
		for (int i = 0; i < nbProcs; i++) {
			PuzzleSolverAlgorithm solver = new WeightingAlgorithm(
					PuzzleSolverConfiguration.generateToleranceParameter(),
					PuzzleSolverConfiguration.generateSizePriorityParameter(),
					PuzzleSolverConfiguration.generateWeightingFragmentationParameter(),
					PuzzleSolverConfiguration.MAX_SQUARE_SIZE);
			launcherList.add(new ThreadedAlgorithm(solver));
		}
	}

	public PuzzleSolution solve(Puzzle puzzle, long computeTimeInMillisecond) {
		
		// Start
		for (ThreadedAlgorithm launcher : launcherList) {
			launcher.start(puzzle);
		}

		// Wait for computation(s)
		try {
			Thread.sleep(computeTimeInMillisecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Stop
		for (ThreadedAlgorithm launcher : launcherList) {
			launcher.stop();
		}

		// Get best result.
		PuzzleSolution bestResult = null;
		for (ThreadedAlgorithm launcher : launcherList) {
			PuzzleSolution result = launcher.getResults();
			if (result != null) {
				if ((bestResult == null) || (bestResult.getNbSquaresFound() > result.getNbSquaresFound())) {
					bestResult = result;
				}
			}
		}
		return bestResult;
	}
}
