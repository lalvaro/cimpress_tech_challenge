package cimpresspuzzle.solver.algo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import cimpresspuzzle.api.Puzzle;
import cimpresspuzzle.api.PuzzleSolution;
import cimpresspuzzle.api.Square;
import cimpresspuzzle.solver.PuzzleSolverAlgorithm;

public class WeightingAlgorithm implements PuzzleSolverAlgorithm {

	private float[] weightingValues;

	private int[] 	workingData;
	private int[] 	maximaGrid;
	private float[] scoreGrid;
	private Random 	rnd;
	private Puzzle 	puzzle;

	private int puzzleWidth;
	private int puzzleHeight;

	private float toleranceParameter;
	private float sizePriorityParameter;
	private int fragmentationParameter;

	@Override
	public String getName() {
		DecimalFormat df = new DecimalFormat("0.0000");
		return getClass().getSimpleName() + " (" + df.format(toleranceParameter) + "|" + df.format(sizePriorityParameter) + "|" + fragmentationParameter + ")";
	}

	public WeightingAlgorithm(float toleranceParameter, float sizePriorityParameter, int fragmentationParameter, int maxSquareSize) {
		this.toleranceParameter 	= toleranceParameter;
		this.sizePriorityParameter 	= sizePriorityParameter;
		this.fragmentationParameter = fragmentationParameter;
		precomputeWeightingValues(maxSquareSize);
		rnd = new Random(System.currentTimeMillis());
	}

	private void precomputeWeightingValues(int maxSquareSize) {
		
		weightingValues = new float[maxSquareSize];

		for (int i = 0; i < maxSquareSize; i++) {
			weightingValues[i] = (float) (1f / Math.pow(i, sizePriorityParameter));
		}
	}

	@Override
	public void init(Puzzle puzzleToSolve) {
		puzzle = puzzleToSolve;
		puzzleWidth = puzzle.getWidth();
		puzzleHeight = puzzle.getHeight();

		int dataSize = puzzleWidth * puzzleHeight;

		workingData = new int[dataSize];
		maximaGrid = new int[dataSize];
		scoreGrid = new float[dataSize];
	}

	@Override
	public PuzzleSolution compute() throws InterruptedException {

		PuzzleSolution solution = new PuzzleSolution();
		System.arraycopy(puzzle.getData(), 0, workingData, 0, puzzle.getData().length);

		int maximumSize = Math.min(puzzleWidth, puzzleHeight);
		runSolver(solution, maximumSize);

		return solution;
	}

	private void runSolver(PuzzleSolution solution, int maximumSize) throws InterruptedException {

		// Chose next square
		while (true) {

			int iterationSize = maximumSize;
			int endSize = 1;

			float lowestCost = Integer.MAX_VALUE;
			Square bestSquare = null;

			ArrayList<Square> tiedForLowestCost = new ArrayList<Square>();

			// mark biggest squares
			while (iterationSize >= endSize) {

				if (Thread.interrupted()) {
					throw new InterruptedException();
				}

				// Parse current grid
				int currentMaxSizeFound = markGreatestSquares(iterationSize);

				// Get the biggest squares <= iterationSize
				ArrayList<Square> biggestSquares = getSquaresBySize(currentMaxSizeFound);

				if (currentMaxSizeFound > 1) {
					for (Square sq : biggestSquares) {

						fillSquare(sq, Puzzle.CELL_BLOCKED);
						float gridCost = 1 + evaluateGridRemainingWeight(iterationSize);
						fillSquare(sq, Puzzle.CELL_EMPTY);

						if (areCostsClose(gridCost, lowestCost)) {
							tiedForLowestCost.add(sq);
						} else if (gridCost < lowestCost) {
							lowestCost = gridCost;
							tiedForLowestCost.clear();
							tiedForLowestCost.add(sq);
						}
					}
				} else {
					if (bestSquare == null) {
						for (Square sq : biggestSquares) {
							solution.addSquare(sq);
						}
						return;
					}
				}

				// set iteration limits
				if (iterationSize == maximumSize) {
					iterationSize = currentMaxSizeFound;
					endSize = Math.max((iterationSize / 2) + 1, 2);
				}
				iterationSize--;
			}

			// Pick a square randomly among the bests
			int randomIndex = rnd.nextInt(tiedForLowestCost.size());
			bestSquare = tiedForLowestCost.get(randomIndex);

			// Mark best square and continue
			fillSquare(bestSquare, Puzzle.CELL_BLOCKED);
			solution.addSquare(bestSquare);

		}
	}

	private boolean areCostsClose(float gridCost, float lowestCost) {
		return (Math.abs(gridCost - lowestCost) / lowestCost) <= toleranceParameter;
	}

	private int markGreatestSquares(int sizeLimit) {

		int cellIndex = 0;
		int maxSizeFound = 0;

		for (int y = 0; y < puzzleHeight; y++) {
			for (int x = 0; x < puzzleWidth; x++) {

				int maxSize = getMaxSquareSize(x, y, sizeLimit);

				maximaGrid[cellIndex] = maxSize;

				if (maxSize > maxSizeFound) {
					maxSizeFound = maxSize;
				}

				cellIndex++;
			}
		}

		return maxSizeFound;
	}

	private float evaluateGridRemainingWeight(int sizeLimit) {

		int cellIndex = 0;
		Arrays.fill(scoreGrid, 0);

		sizeLimit = Math.min(sizeLimit, fragmentationParameter);

		for (int y = 0; y < puzzleHeight; y++) {
			for (int x = 0; x < puzzleWidth; x++) {

				if (workingData[cellIndex] != Puzzle.CELL_BLOCKED) {

					int sizeFound = getMaxSquareSize(x, y, sizeLimit);
					maximaGrid[cellIndex] = sizeFound;

					markScoreGrid(x, y, sizeFound, weightingValues[sizeFound]);
				}

				cellIndex++;
			}
		}

		float weight = 0;

		for (int i = 0; i < scoreGrid.length; i++) {
			weight += scoreGrid[i];
		}

		return weight;
	}


	private void fillSquare(Square sq, int value) {
		int xstart = sq.x;
		int ystart = sq.y;
		int xend = xstart + sq.size;
		int yend = ystart + sq.size;

		for (int y = ystart; y < yend; y++) {
			Arrays.fill(workingData, xstart + (y * puzzleWidth), xend + (y * puzzleWidth), value);
		}
	}

	private void markScoreGrid(int originx, int originy, int size, float value) {
		int xstart = originx;
		int ystart = originy;
		int xend = xstart + size;
		int yend = ystart + size;

		for (int x = xstart; x < xend; x++) {
			for (int y = ystart; y < yend; y++) {

				int index = x + (y * puzzleWidth);

				if ((scoreGrid[index] == 0) || (scoreGrid[index] > value)) {
					scoreGrid[index] = value;
				}
			}
		}
	}

	private ArrayList<Square> getSquaresBySize(int searchedSize) {

		ArrayList<Square> output = new ArrayList<Square>();
		int dataSize = puzzleWidth * puzzleHeight;

		for (int i = 0; i < dataSize; i++) {
			if (maximaGrid[i] == searchedSize) {
				output.add(new Square(i % puzzleWidth, i / puzzleWidth, searchedSize));
			}
		}

		return output;
	}

	private int getMaxSquareSize(int originx, int originy, int sizeLimit) {

		int originIndex = originx + (originy * puzzleWidth);

		if ((workingData[originIndex] == Puzzle.CELL_BLOCKED)) {
			return 0;
		}

		int maximumPossibleSize = Math.min(puzzleWidth - originx, puzzleHeight - originy);
		int maximumSizeToBeTested = Math.min(maximumPossibleSize, sizeLimit);

		// optimization : if previous square was n*n, the current cell is at least n-1*n-1
		int startSize = 1;

		if (originx > 0) {
			startSize = Math.max(1, maximaGrid[originIndex - 1]);
		} else {
			if (originy > 0) {
				startSize = Math.max(1, maximaGrid[originIndex - puzzleWidth]);
			}
		}

		for (int currentSize = startSize; currentSize <= maximumSizeToBeTested; currentSize++) {

			int verticalIndex = originIndex + (currentSize - 1);
			int horizontalIndex = originIndex + ((currentSize - 1) * puzzleWidth);

			for (int i = 0; i < (currentSize - 1); i++) {
				if ((workingData[horizontalIndex + i] == Puzzle.CELL_BLOCKED)
						|| (workingData[verticalIndex + (i * puzzleWidth)] == Puzzle.CELL_BLOCKED)) {
					return currentSize - 1;
				}
			}

			if ((workingData[(horizontalIndex + currentSize) - 1] == Puzzle.CELL_BLOCKED)) {
				return currentSize - 1;
			}
		}

		return maximumSizeToBeTested;
	}
}
