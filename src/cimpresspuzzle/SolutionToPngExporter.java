package cimpresspuzzle;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import cimpresspuzzle.api.Square;

public class SolutionToPngExporter {

	private static final int PIXEL_SIZE 	= 10;
	private static final int BORDER_WIDTH  	= 1;

	public static void export(int width, int height, ArrayList<Square> solution, String exportName) {

		int imageWidth  = (width * PIXEL_SIZE) + BORDER_WIDTH;
		int imageHeight = (height* PIXEL_SIZE) + BORDER_WIDTH;

		try {

			BufferedImage bi  = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D 	ig2 = bi.createGraphics();

			// background
			ig2.setPaint(Color.WHITE);
			ig2.fillRect(0, 0, imageWidth, imageHeight);

			Random rnd = new Random(0);

			// draw squares
			for(Square sq : solution) {

				float r = 0.25f + (rnd.nextFloat() * 0.5f);
				float g = 0.25f + (rnd.nextFloat() * 0.5f);
				float b = 0.25f + (rnd.nextFloat() * 0.5f);

				ig2.setPaint(new Color(r, g, b));

				for (int x=sq.x; x<(sq.x+sq.size); x++) {
					for(int y=sq.y; y<(sq.y+sq.size); y++) {
						ig2.fillRect(BORDER_WIDTH+(x*PIXEL_SIZE), BORDER_WIDTH+(y*PIXEL_SIZE), PIXEL_SIZE, PIXEL_SIZE);
					}
				}
			}

			// draw grid
			ig2.setPaint(Color.WHITE);
			for (int x=0; x<width; x++) {
				ig2.drawLine(x*PIXEL_SIZE, 0, x*PIXEL_SIZE, height*PIXEL_SIZE);
			}
			for (int y=1; y<height; y++) {
				ig2.drawLine(0, y*PIXEL_SIZE, width*PIXEL_SIZE, y*PIXEL_SIZE);
			}

			// draw squares borders
			for(Square sq : solution) {
				ig2.setPaint(Color.BLACK);
				ig2.drawRect(sq.x*PIXEL_SIZE, sq.y*PIXEL_SIZE, sq.size*PIXEL_SIZE, sq.size*PIXEL_SIZE);
			}

			exportName = exportName.replaceAll(" ", "_").replaceAll("\\|", ";").replaceAll("\\(", "[[").replaceAll("\\)", "]]");
			File exportFile = new File(exportName + "[" + solution.size() + "].png");
			exportFile.getParentFile().mkdirs();
			ImageIO.write(bi, "PNG", exportFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
