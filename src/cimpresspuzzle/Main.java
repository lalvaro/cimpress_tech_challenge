package cimpresspuzzle;

import cimpresspuzzle.api.CimpressPuzzleAPI;
import cimpresspuzzle.api.Puzzle;
import cimpresspuzzle.api.PuzzleSolution;
import cimpresspuzzle.solver.PuzzleSolver;

public class Main {

	private static final int TIME_BUDGET_IN_MILLIS 	= 9000;
	private static final int NB_PUZZLES_TO_SOLVE	= 400;	
	
	public static void main(String[] args) {

		PuzzleSolver solver = new PuzzleSolver();

		for (int i = 0; i < NB_PUZZLES_TO_SOLVE; i++) {

			Puzzle puzzle = CimpressPuzzleAPI.getNextPuzzle();
			System.out.println("Puzzle running[" + i + "]: " + puzzle.getId());

			PuzzleSolution result = solver.solve(puzzle, TIME_BUDGET_IN_MILLIS);

			if (result != null) {
				CimpressPuzzleAPI.sendPuzzleSolution(result, puzzle.getId());
			} else {
				System.err.println("Time too short to find a solution.");
			}
		}
	}
}
